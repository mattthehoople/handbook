# Admin

How to do general work admin things

* [Holidays](holidays.md)
* [Being sick](sickness.md)
* [Getting paid](money.md)
* [Learning](learning.md)
* [Staying healthy](health.md)
* [Travel policy](policy.md)