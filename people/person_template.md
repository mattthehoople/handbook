# Person's name

![Person's photo][link to photo]

## Details

* Start date - 
* Leaving date -
* Contact email - 

## Role(s)

* [Role](link to role) in [Team](link to team)
* [Role](link to role) in [Team](link to team)

### Previous roles

* Was [Role](link to role) in [Team](link to team) during (rough time scale)
* Was [Role](link to role) in [Team](link to team) during (rough time scale)

## Bio

Short bio

## Can help you with...

List of work things this person can help you with

* [Project 1](../work/work_template.md)
* [Admin thing](../admin/admin_template.md)