# Company Handbook

Contents

* [Getting Started](getting_started/README.md)
* [Ethos](ethos/README.md)
* [Roles](roles/README.md)
* [People](people/README.md)
* [Our work](work/README.md)
* [Working Practices](working_practices/README.md)
* [Admin Stuff](admin/README.md)