# Working practices

How we do things day-to-day.

## Defining work

How we find and evaluate ideas

## Delivering work

How we get those ideas into the hands of our customers

## Measuring success

How we know if our ideas are any good