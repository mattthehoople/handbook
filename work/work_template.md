# Product/Feature/Project/Initiative name

Ongoing or delivered

## Rationale

What this addresses

## Useful links

These things are likely to be on other systems

## Useful People

These people can help you learn about this peice of work

* [Role](../roles/role_template.md) - [Person](../people/person_template.md)