# Getting Started

Welcome! Here are the things you need to hit the ground running

* [Getting Access to things](./access.md)
* [Orientation](./orientation.md)
* [Your first day](./first_day.md)
* [Your first week](./first_week.md)